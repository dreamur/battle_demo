/***********
 *
 * TODO:
 *
 * LATER:
 **********/

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Audio/Music.hpp>
#include <stdio.h>
#include <cstdlib>
#include "utility.h"

#define MS_PER_UPDATE 2

using utilityMethods:: cursorState;
using utilityMethods:: onBoss;
using utilityMethods:: currentTurn;

void listener (sf:: Event & e, cursorState& c, onBoss& obEnum, currentTurn& cTObj);
void update ();

void switchBGM (std:: string songKey, rawData* dataObj);
void initState (rawData* dataObj);

void drawFn(sf:: RenderWindow& window)
{
    for (auto pair : liveData:: renderSprites) { window.draw(*pair.second); }
    for (auto pair : liveData:: renderText)    { window.draw(*pair.second); }
}

int main()
{
    cursorState cursorObj = cursorState:: POS1;
    onBoss onBossEnum = onBoss:: NEA;
    currentTurn currentTurnObj = currentTurn:: HERO;

    rawData dataObj;
    initState(&dataObj);

    // 1280 x 720
    sf:: RenderWindow renderWindow(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Demo Game");

    liveData:: battleTimer.restart();

    double previous = liveData:: battleTimer.getElapsedTime().asMilliseconds();
    double lag      = 0.0;

    while (renderWindow.isOpen())
    {
        double current = liveData:: battleTimer.getElapsedTime().asMilliseconds();
        double elapsed = current - previous;

        previous = current;
        lag += elapsed;

        sf::Event event;

        while (renderWindow.pollEvent(event))
        {
            if (event.type == sf:: Event:: Closed)
                renderWindow.close();
            if (event.type == sf:: Event:: KeyPressed)
                listener(event, cursorObj, onBossEnum, currentTurnObj);
        }

        while (lag >= MS_PER_UPDATE)
        {
            update();
            lag -= MS_PER_UPDATE;
        }

        drawFn(renderWindow);

        renderWindow.display();

    }
    return 0;
}

void listener(sf:: Event& e, cursorState& c, onBoss& obEnum, currentTurn& cTObj)
{
    if (cTObj == currentTurn:: BOSS && e.type != sf:: Event:: KeyPressed && e.type != sf:: Event:: KeyReleased) { /*handleBossTurn(cTObj);*/ return; }
    else
    {
        if (e.type == sf:: Event:: KeyPressed)
        {
            processInput(e, c, obEnum, cTObj);
            //utilityMethods:: fillLabelBox(c, obEnum);                
        }
    }
}

void update()
{
    for (auto pair : liveData:: animList)
    {
        //std:: cout << pair.first << ' ';
        //std:: cout << "update is called\n";
        //animation tmpAnimation = *pair.second;
        //tmpAnimation.update(); //std:: cout << "animation updated\n";

        /*if (tmpAnimation.age >= tmpAnimation.lifeTime)
        {
            if (tmpAnimation.utilFn != nullptr)
                liveData:: renderSprites.emplace(tmpAnimation.spriteString, tmpAnimation.moveableSprite);

            if (tmpAnimation.moveableText != nullptr)
            {
                for (auto p : liveData:: renderText)
                {
                    if (p.second == tmpAnimation.moveableText)
                        liveData:: renderText.erase(p.first);
                }
            }
            liveData:: animList.erase(pair.first);
            //std:: cout << "an animation has been removed ";
            //std:: cout << "animation list size:: " << liveData:: animList.size() << '\n';
        }*/
    } //std:: cout << '\n';
}

void switchBGM(std:: string songKey, rawData* dataObj)
{
    static sf:: Music currentSong;

    if (songKey == "BATTLELOOP")
    {
        currentSong.openFromFile(dataObj->paths["BATTLELOOP"]);
        currentSong.setLoop(true);
    }
    std:: cout << (int)currentSong.getStatus() << '\n';
    currentSong.play();
}

void initState(rawData* dataObj)
{
    utilityMethods:: setupBattleScene(dataObj);
}
