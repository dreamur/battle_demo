/*************************
 *
 * ACTOR header
 *
 * BRIEF:
 *
 *
 * TODO:
 *
 *
 ***************************/

#ifndef __ACTOR_H__
#define __ACTOR_H__

#include "data.h"

class actor
{

    public:
        std:: shared_ptr<sf:: Sprite> actorImage;
        std:: shared_ptr<sf:: Sprite> hpBarImage;
        int healthPool;   // this is raw HP value (before hpbar's width is converted)

        actor() = default;
        actor(std:: string actorKeyName, std:: string hpBarKeyName, int hp);
        actor(const actor& actorTwo);
        actor& operator = (const actor& actorTwo);
        ~actor() = default;
};

inline actor& actor:: operator = (const actor& actorTwo)
{
    if (this != &actorTwo)
    {
        actorImage.reset();
        hpBarImage.reset();

        actorImage = actorTwo.actorImage;
        hpBarImage = actorTwo.hpBarImage;
        healthPool = actorTwo.healthPool;
    }

    return *this;
}

inline actor:: actor(const actor& actorTwo)
{
    actorImage = actorTwo.actorImage;
    hpBarImage = actorTwo.hpBarImage;
    healthPool = actorTwo.healthPool;
}

inline actor:: actor(std:: string actorKeyName, std:: string hpBarKeyName, int hp)
{
    actorImage = liveData:: spriteList.at(actorKeyName);
    hpBarImage = liveData:: spriteList.at(hpBarKeyName);
    healthPool = hp;
}

#endif
