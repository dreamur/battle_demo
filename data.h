/*************************
 *
 * Raw Data header
 *
 * BRIEF:
 *
 *
 * TODO:
 *
 *
 ***************************/

#ifndef __RAWDATA_H__
#define __RAWDATA_H__

#include <string>
#include <map>
#include <memory>
#include <random>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/System/Clock.hpp>

class animation;
class actor;

struct rawData
{
    std:: map<std:: string, std:: string> paths
    {
        //                     //
        // TEXTURES START HERE //
        //                     //

        {"HERO",              "./assets/actors/0__current_project/hero.png"},
        {"MILITARYCHICK",     "./assets/actors/0__current_project/militaryChick.png"},
        {"ICONS",             "./assets/guis/0__current_project/icons.png"},
        {"GUI",               "./assets/guis/0__current_project/uipack_rpg_sheet.png"},
        {"MONSTERS",          "./assets/actors/0__current_project/monsters-001.png"},
        {"HPBAR",             "./assets/guis/0__current_project/hpBar.png"},

        {"PARALLAX000", "./assets/backgrounds/0__current_project/skyline-000a.png"},
        {"PARALLAX001", "./assets/backgrounds/0__current_project/skyline-001a.png"},
        {"PARALLAX002", "./assets/backgrounds/0__current_project/skyline-002a.png"},
        {"PARALLAX003", "./assets/backgrounds/0__current_project/skyline-003a.png"},

        //                  //
        //  SFX STARTS HERE //
        //                  //

        {"MENUSELECT",      "./assets/sounds/0__current_project/menu_select.wav"},
        {"BLIP",            "./assets/sounds/0__current_project/blip.wav"},
        {"HEROSTRIKE",      "./assets/sounds/0__current_project/drawKnife3.ogg"},
        {"MONSTERSTRIKE",   "./assets/sounds/0__current_project/monster-15.wav"},

        //                    //
        //  MUSIC STARTS HERE //
        //                    //

        {"ROAMINGTUNE", "./assets/music/0__current_project/Ove Melaa - Times.mp3"},
        {"BATTLELOOP", "./assets/music/0__current_project/Ove Melaa - Supa Powa Loop A.ogg"},
        //{"FINALBOSSTHEME", "./assets/music/0__current_project/DarkWinds.OGG"},

        //                   //
        //  FONTS START HERE //
        //                   //

        {"GLOBALTEXT", "./assets/fonts/0__current_project/8-BIT WONDER.TTF"}

    };

    std:: map<std:: string, sf:: IntRect> imgSrcRects
    {
        {"HEROBATTLE", sf:: IntRect(108, 1, 27, 62)},
        {"MILITARYCHICKBATTLE", sf:: IntRect(195, 4, 27, 59)},

        {"EYEMON", sf:: IntRect(100, 137, 45, 46)},
        {"TEXTRECT", sf:: IntRect(0, 237, 190, 45)},
        {"CURSOR", sf:: IntRect(325, 486, 22, 21)},
        //{"HPBARSEG", sf:: IntRect(356, 368, 18, 18)},
        //{"HPBAREND", sf:: IntRect(190, 348, 9, 18)},
        {"HPBARFULL", sf:: IntRect(0, 0, 200, 41)},

        {"ICONSHIELD", sf:: IntRect(128, 0, 128, 128)},
        {"ICONFIST", sf:: IntRect(384, 0, 128, 128)},
        {"ICONBOOK", sf:: IntRect(256, 0, 128, 128)},
        {"ICONRUN", sf:: IntRect(2176, 0, 128, 128)}
    };

};

namespace liveData
{
    //-- LUTs to be filled at runtime --//
    static std:: map<std:: string, sf:: IntRect> rectList;
    static std:: map<std:: string, sf:: Texture> texList;

    // spriteList    != renderSprites
    // spriteList    == ALL the renderable sprite objects for a scene
    // renderSprites == ONLY what is necessary to render
    static std:: map<std:: string, std:: shared_ptr <sf:: Sprite> > spriteList;
    static std:: map<std:: string, std:: shared_ptr <sf:: Sprite> > renderSprites;

    static std:: map<std:: string, sf:: SoundBuffer> buffersList;
    static std:: map<std:: string, sf:: Sound> soundsList;

    static std:: map<std:: string, sf:: Font> fontsList;
    static std:: map<std:: string, std:: shared_ptr<sf:: Text> > textObjsList;
    static std:: map<std:: string, std:: shared_ptr<sf:: Text> > renderText;

    static std:: map<std:: string, std:: shared_ptr<animation> > animList;

    static std:: map<std:: string, std:: shared_ptr<actor> > actorList;

    // random_device is a quick and easy way to grab a random number (good for seeding)
    std:: random_device r;
    static std:: default_random_engine generator( r() );
    static std:: bernoulli_distribution rng_coin(0.5);
    static std:: normal_distribution<> hero_dmg{15,1.5};
    static std:: normal_distribution<> heroine_dmg{13, 3};
    static std:: normal_distribution<> boss_dmg{15, 3};

    static sf:: Clock battleTimer;
};

#endif
