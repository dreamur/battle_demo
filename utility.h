/*************************
 *
 * Utility Methodds Namespace
 *
 * BRIEF:
 *
 *  + This was made solely to keep
 *    the main file terse
 *
 * TODO:
 *
 *
 ***************************/

#ifndef __UTILITY_H__
#define __UTILITY_H__

#include "animation.h"
#include <SFML/Window/Event.hpp>
#include <stdexcept>
#include <algorithm>

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

namespace utilityMethods
{
    enum class cursorState { POS1 = 0, POS2, POS3, POS4 };
    enum class onBoss { NEA = 0, YEA };
    enum class currentTurn {  HERO = 0, HEROINE, BOSS };

    void playSound (std:: string soundLabel)
    {
        liveData:: soundsList.at(soundLabel).play();
    }

    bool isLivingSprite(std:: string keyName)
    {
        for (auto pair : liveData:: renderSprites)
            if (pair.first == keyName)
                return true;
        return false;
    }

    bool isLivingAnim(std:: string keyName)
    {
        for (auto pair : liveData:: animList)
            if (pair.first == keyName)
                return true;
        return false;
    }

    bool isLivingActor(std:: string keyName)
    {
        for (auto pair : liveData:: actorList)
            if (pair.first == keyName)
                return true;
        return false;
    }

    bool isLivingText(std:: string keyName)
    {
        for (auto pair : liveData:: renderText)
            if (pair.first == keyName)
                return true;
        return false;
    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    void createMoveAnimation(std:: string targetMoveable, unsigned animDuration, int moveX, int moveY, std:: string desiredAnimName, bool isTextObj = false)
    {
        std:: shared_ptr<animation> tempMove;
        if (!isTextObj)
            tempMove = std:: make_shared<animation>(targetMoveable, animDuration, true);
        else
            tempMove = std:: make_shared<animation>(targetMoveable, animDuration, false, true);

        //sf:: Clock* clockPtr = &tempMove->animTimer;
        std:: function<void(int, int*, int*, int, int)> dropFn;

        if (tempMove->moveableSprite != nullptr)
        {
            std:: shared_ptr<sf:: Sprite> moveablePtr = std:: make_shared<sf:: Sprite>();
            moveablePtr = tempMove->moveableSprite;

            dropFn = [targetMoveable, moveablePtr](int frequency, int* paramAge, int* paramTime, int moveX, int moveY)
            {
                int newTime = liveData:: battleTimer.getElapsedTime().asMilliseconds();
                int newAge  = newTime - *paramTime + *paramAge;
                *paramTime = newTime;
                *paramAge += newAge;

                if (*paramAge % frequency == 0)
                {
                    sf:: FloatRect moveableRect = moveablePtr->getGlobalBounds();
                    moveablePtr->setPosition(moveableRect.left + moveX, moveableRect.top + moveY);
                }
            };
        }
        else
        {
            std:: shared_ptr<sf:: Text> moveablePtr = std:: make_shared<sf:: Text>();
            moveablePtr = tempMove->moveableText;

            dropFn = [targetMoveable, moveablePtr](int frequency, int* paramAge, int* paramTime, int moveX, int moveY)
            {
                int newTime = liveData:: battleTimer.getElapsedTime().asMilliseconds();
                int newAge  = newTime - *paramTime + *paramAge;
                *paramTime = newTime;
                *paramAge += newAge;

                if (*paramAge % frequency == 0)
                {
                    sf:: FloatRect moveableRect = moveablePtr->getGlobalBounds();
                    moveablePtr->setPosition(moveableRect.left + moveX, moveableRect.top + moveY);
                }
            };
        }

        int newTime = liveData:: battleTimer.getElapsedTime().asMilliseconds();
        int newAge  = newTime - tempMove->previousTime + tempMove->age;
        tempMove->previousTime = newTime;
        tempMove->age = newAge + tempMove->age;

        tempMove->moveFn = dropFn;
        tempMove->setFrequency(20);
        tempMove->xOffset = moveX;
        tempMove->yOffset = moveY;

        liveData:: animList.emplace( desiredAnimName, tempMove );
    }

    void createBlinkAnimation(std:: string targetSprite, unsigned animDuration, std:: string desiredAnimName)
    {
        // add 'hurt' animation to monster here
        std:: shared_ptr<animation> tempBlink = std:: make_shared<animation>(targetSprite, animDuration, true);
        std:: shared_ptr<sf:: Sprite> spritePtr = std:: make_shared<sf:: Sprite>();
        spritePtr = tempBlink->moveableSprite;

        //std:: cout << "im in the createBlinkAni()\n";

        std::function<void(int, int*, int*)> blinkFn = [targetSprite, spritePtr](int frequency, int* paramAge, int* paramTime)
        {
            int newTime = liveData:: battleTimer.getElapsedTime().asMilliseconds();
            int newAge  = newTime - *paramTime + *paramAge;
            *paramTime = newTime;
            *paramAge += newAge;
            //std:: cout << "blink lambda called\n";
            if (*paramAge % frequency == 0)
            {

                auto it = liveData:: renderSprites.find(targetSprite);
                if (it != liveData:: renderSprites.end()) {
                    liveData:: renderSprites.erase(it);
                }
                else
                    liveData:: renderSprites.emplace(targetSprite, spritePtr);
            }
        };

        int newTime = liveData:: battleTimer.getElapsedTime().asMilliseconds();
        int newAge  = newTime - tempBlink->previousTime + tempBlink->age;
        tempBlink->previousTime = newTime;
        tempBlink->age = newAge + tempBlink->age;

        tempBlink->utilFn = blinkFn;
        tempBlink->setFrequency(50);
        // NOTE:: the animation's copy ctr is called dozens of times w/
        // each keystroke. this was causing a memory leak
        liveData:: animList.emplace( desiredAnimName, tempBlink );
    }

    void updateBattleTurn(currentTurn& cTObj)
    {
        cTObj = (currentTurn)(((int)cTObj + 1) % 2);
    }

    int calcDamage(std:: string keyName)
    {
        int damage;
        if(keyName == "011_HEROBATTLE")
        {
            damage = (int)liveData:: hero_dmg(liveData:: generator);
        }
        else if (keyName == "012_ARMYGIRLBATTLE")
        {
            damage = (int)liveData:: heroine_dmg(liveData:: generator);
        }
        else if (keyName == "010_EYEBALL")
        {
            damage = (int)liveData:: boss_dmg(liveData:: generator);
        }
        //std:: cout << damage << '\n';
        return damage;
    }

    bool resizeHPBar(std:: string keyName, float damage, float hpScalar)
    {
        if ( !isLivingSprite(keyName) )
        {
            std:: cerr << keyName << " (hpBar) does not exist\n";
            return false;
        }

        float currentHP, currentWidth, trueWidth, yScale;

        trueWidth       = liveData:: renderSprites.at(keyName)->getLocalBounds().width;
        currentWidth    = liveData:: renderSprites.at(keyName)->getGlobalBounds().width;

        yScale          = liveData:: renderSprites.at(keyName)->getScale().y;
        currentHP       = currentWidth / hpScalar;

        currentWidth = (currentHP - damage) * hpScalar;

        if ( (currentHP - damage) <= 0)
        {
            for (auto actorPair : liveData:: actorList)
            {
                if ( actorPair.second->hpBarImage == liveData:: renderSprites.at(keyName) )
                {
                    for (auto spritePair : liveData:: renderSprites)
                        if (spritePair.second == actorPair.second->actorImage)
                            liveData:: renderSprites.erase(spritePair.first);

                    for (auto animPair : liveData:: animList)
                        if (animPair.second->moveableSprite == actorPair.second->actorImage)
                            liveData:: animList.erase(animPair.first);

                    liveData:: actorList.erase(actorPair.first);
                }
            }
            liveData:: renderSprites.erase(keyName);
            return false;
        }
        else { liveData:: renderSprites.at(keyName)->setScale(currentWidth / trueWidth, yScale); return true; }
    }

    void createDamageText(int damage, std:: string hurtTarget)
    {
        std:: string dmg = std:: to_string(damage);
        std:: shared_ptr<sf:: Text> damageTxt = std:: make_shared<sf:: Text>(dmg, liveData:: fontsList.at("GLOBALFONT") );
        sf:: Vector2f actorPos = liveData:: actorList.at(hurtTarget)->actorImage->getPosition();
        damageTxt->setPosition(actorPos.x, actorPos.y);
        liveData:: renderText.emplace(dmg, damageTxt);
        createMoveAnimation(dmg, 1000, 0, 10, dmg, true);
    }

    void handleBossTurn(currentTurn& cTObj)
    {
        if (cTObj != currentTurn:: BOSS) { return; }

        int dmg = calcDamage("010_EYEBALL");
        std:: string spriteKey, animKey, hpKey;

        if (liveData:: rng_coin(liveData:: generator) == true) { spriteKey = "011_HEROBATTLE";     animKey = "HEROHURTANIM";    hpKey = "037_HEROHP"; }
        else                                                   { spriteKey = "012_ARMYGIRLBATTLE"; animKey = "HEROINEHURTANIM"; hpKey = "038_HEROINEHP"; }


        //std:: cout << "handleBossTurn() called\n";
        if ( resizeHPBar(hpKey, dmg, 2) )
        {
            createBlinkAnimation(spriteKey, 1000, animKey);
            createDamageText(dmg, spriteKey);
        }
        updateBattleTurn(cTObj);
    }


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    void fillLabelBox (cursorState cStateObj, onBoss& obEnum)
    {
        int tempState = ((int)cStateObj + 4) % 4;

        liveData:: renderText.erase("ATTACK");
        liveData:: renderText.erase("GUARD");
        liveData:: renderText.erase("MAGIC");
        liveData:: renderText.erase("FLEE");
        liveData:: renderText.erase("OCULUS");
        liveData:: renderSprites.emplace("036_STATUSNAME", liveData:: spriteList["036_STATUSNAME"]);

        sf:: FloatRect boxBounds = liveData:: renderSprites.at("036_STATUSNAME")->getLocalBounds();

        float trueBoxWidth = (boxBounds.left + boxBounds.width);
        float trueBoxHeight = (boxBounds.top + boxBounds.height);

        std:: string textString;

        if (tempState == 0)
        {
            if (obEnum == onBoss:: NEA)
            {
                liveData:: renderText.emplace("ATTACK", liveData:: textObjsList["ATTACK"]);
                textString = "ATTACK";
            }
            else
            {
                liveData:: renderText.emplace("OCULUS", liveData:: textObjsList["OCULUS"]);
                textString = "OCULUS";
            }
        }
        else if (tempState  == 1)
        {
            liveData:: renderText.emplace("GUARD", liveData:: textObjsList["GUARD"]);
            textString = "GUARD";
        }
        else if (tempState  == 2)
        {
            liveData:: renderText.emplace("MAGIC", liveData:: textObjsList["MAGIC"]);
            textString = "MAGIC";
        }
        else if (tempState  == 3)
        {
            liveData:: renderText.emplace("FLEE", liveData:: textObjsList["FLEE"]);
            textString = "FLEE";
        }
        else
        {
            liveData:: renderText.erase("ATTACK");
            liveData:: renderText.erase("GUARD");
            liveData:: renderText.erase("MAGIC");
            liveData:: renderText.erase("FLEE");
        }

        sf:: FloatRect textBounds = liveData:: renderText.at(textString)->getLocalBounds();
        sf:: FloatRect textGlobal = liveData:: renderText.at(textString)->getGlobalBounds();
        liveData:: renderSprites.at("036_STATUSNAME")->setScale( (textBounds.width+14)/trueBoxWidth, (textBounds.height+14)/trueBoxHeight );
        liveData:: renderSprites.at("036_STATUSNAME")->setPosition(textGlobal.left, textGlobal.top);
        liveData:: renderSprites.at("036_STATUSNAME")->move(-6, -6);
    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    //void scrollParallax(int& timeInMilli); now defunct - animations will take its place

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    void selectChoice(cursorState& c, onBoss& obEnum, currentTurn& cTObj)
    {
        bool failFlag = false;
        if ( !isLivingSprite("009_BOSSHP") )
        {
            std:: cerr << "009_BOSSHP (sprite) does not exist within selectChoice() method\n";
            failFlag = true;
        }
        if ( !isLivingSprite("010_EYEBALL") )
        {
            std:: cerr << "010_EYEBALL (sprite) does not exist within selectChoice() method\n";
            failFlag = true;
        }
        if ( !isLivingActor("010_EYEBALL") )
        {
            std:: cerr << "010_EYEBALL (actor) does not exist within selectChoice() method\n";
            failFlag = true;
        }

        if ((int)c == 0)
        {
            obEnum = obEnum == onBoss:: YEA ? onBoss:: NEA : onBoss:: YEA;

            if (obEnum == onBoss:: YEA)
            {
                if ( !failFlag )
                {
                    sf:: FloatRect coords = liveData:: actorList.at("010_EYEBALL")->actorImage->getGlobalBounds();
                    liveData:: renderSprites.at("035_CURSOR")->setPosition( (coords.left + coords.left + coords.width) / 2, coords.top + 10);
                    liveData:: renderSprites.at("035_CURSOR")->setRotation(90);
                    playSound("MENUSELECT");
                }
            }
            else
            {
                sf:: Vector2f coords = liveData:: renderSprites.at("031_ATKICON")->getPosition();
                liveData:: renderSprites.at("035_CURSOR")->setPosition(coords.x, coords.y);
                liveData:: renderSprites.at("035_CURSOR")->setRotation(0);
                playSound("HSTRIKE");

                int damage;
                if (cTObj == currentTurn:: HERO) { damage = utilityMethods:: calcDamage("011_HEROBATTLE"); }
                else if (cTObj == currentTurn:: HEROINE) { damage = utilityMethods:: calcDamage("012_ARMYGIRLBATTLE"); }

                if ( !failFlag && resizeHPBar("009_BOSSHP", damage, 1.333333) )
                {            
                    createDamageText(damage, "010_EYEBALL");
                    createBlinkAnimation("010_EYEBALL", 1000, "MONSTERHURTANIM");
                }
                else std:: cout << "monster deleted\n";
                updateBattleTurn(cTObj);
                //std:: cout << "monster has been hit!!!!!\n";
            }
        }
        else if ((int)c == 1)
        {
            std:: cout << "RAISING DEFENSES!\n";
            playSound("MENUSELECT");
        }
        else if ((int)c == 2)
        {
            std:: cout << "CASTING SPELL!\n";
            playSound("MENUSELECT");
        }
        else if ((int)c == 3)
        {
            std:: cout << "CAN'T RUN!\n";
            playSound("MENUSELECT");
        }
    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    void processInput(sf:: Event& e, cursorState& c, onBoss& obEnum, currentTurn& cTObj)
    {
        if ( !isLivingSprite("035_CURSOR") || !isLivingSprite("031_ATKICON") )
        {
            std:: cerr << "cursor or attack icon does not exist" << '\n';
            return;
        }
        if (e.key.code == sf:: Keyboard:: Right or e.key.code == sf:: Keyboard:: Down)
        {
            if (obEnum == onBoss:: NEA)
            {
                int temp = (((int)c + 1) % 4);
                c = (cursorState)temp;
                temp = (temp + 4) % 4;

                liveData:: renderSprites.at("035_CURSOR")->setPosition( liveData:: renderSprites.at("035_CURSOR")->getPosition().x , (81 + 148 * temp) % (148 * 4) );
                playSound("BLIPSOUND");
            }
        }
        else if (e.key.code == sf:: Keyboard:: Left or e.key.code == sf:: Keyboard:: Up)
        {
            if (obEnum == onBoss:: NEA)
            {
                int temp = (((int)c - 1) % 4);
                c = (cursorState)temp;
                temp = (temp + 4) % 4;

                liveData:: renderSprites.at("035_CURSOR")->setPosition( liveData:: renderSprites.at("035_CURSOR")->getPosition().x , abs((81 + 148 * temp) % (148 * 4)) );
                playSound("BLIPSOUND");
            }
        }
        else if (e.key.code == sf:: Keyboard:: Return) { selectChoice(c, obEnum, cTObj);  return; }
        else if (e.key.code == sf:: Keyboard:: Escape)
        {
            obEnum = (onBoss)0;

            sf:: Vector2f coords = liveData:: renderSprites.at("031_ATKICON")->getPosition();
            liveData:: renderSprites.at("035_CURSOR")->setPosition(coords.x, coords.y);
            liveData:: renderSprites.at("035_CURSOR")->setRotation(0);
            //utilityMethods:: playSound("HSTRIKE"); we could use a different sound... just say'in
        }
    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    void createActorObject(std:: string actorKeyName, std:: string hpBarKeyName, int hp)
    {
        std:: shared_ptr<actor> tmp = std:: make_shared<actor>(actorKeyName, hpBarKeyName, hp);
        liveData:: actorList.emplace( actorKeyName, std:: move(tmp) );
    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    void setupBattleScene(rawData* dataObj)
    {
        sf:: Font font;
        sf:: Texture battle_bak_000;
        sf:: Texture battle_bak_001;
        sf:: Texture battle_bak_002;
        sf:: Texture battle_bak_003;

        sf:: Texture hero_sheet;
        sf:: Texture militaryChick_sheet;
        sf:: Texture monster_sheet;
        sf:: Texture gui_sheet;
        sf:: Texture icon_sheet;
        sf:: Texture hpBar_sheet;

        font.loadFromFile(dataObj->paths["GLOBALTEXT"]);
        battle_bak_000.loadFromFile(dataObj->paths["PARALLAX000"]);
        battle_bak_001.loadFromFile(dataObj->paths["PARALLAX001"]);
        battle_bak_002.loadFromFile(dataObj->paths["PARALLAX002"]);
        battle_bak_003.loadFromFile(dataObj->paths["PARALLAX003"]);
        hero_sheet.loadFromFile(dataObj->paths["HERO"]);
        militaryChick_sheet.loadFromFile(dataObj->paths["MILITARYCHICK"]);
        monster_sheet.loadFromFile(dataObj->paths["MONSTERS"]);
        gui_sheet.loadFromFile(dataObj->paths["GUI"]);
        icon_sheet.loadFromFile(dataObj->paths["ICONS"]);
        hpBar_sheet.loadFromFile(dataObj->paths["HPBAR"]);

        liveData:: fontsList.emplace("GLOBALFONT", font);
        liveData:: texList.emplace("PARALLAX000", battle_bak_000);
        liveData:: texList.emplace("PARALLAX001", battle_bak_001);
        liveData:: texList.emplace("PARALLAX002", battle_bak_002);
        liveData:: texList.emplace("PARALLAX003", battle_bak_003);
        liveData:: texList.emplace("HERO", hero_sheet);
        liveData:: texList.emplace("ARMYCHICK", militaryChick_sheet);
        liveData:: texList.emplace("MONSTERS", monster_sheet);
        liveData:: texList.emplace("GUI", gui_sheet);
        liveData:: texList.emplace("ICONS", icon_sheet);
        liveData:: texList.emplace("HPBAR", hpBar_sheet);

        sf:: IntRect heroRect(dataObj->imgSrcRects["HEROBATTLE"]);

        liveData:: rectList.emplace("HERORECT", heroRect);

        std:: shared_ptr<sf:: Text> atkTxt   = std:: make_shared<sf:: Text>("ATTACK", liveData:: fontsList["GLOBALFONT"]);
        std:: shared_ptr<sf:: Text> defTxt   = std:: make_shared<sf:: Text>("GUARD", liveData:: fontsList["GLOBALFONT"]);
        std:: shared_ptr<sf:: Text> magTxt   = std:: make_shared<sf:: Text>("MAGIC", liveData:: fontsList["GLOBALFONT"]);
        std:: shared_ptr<sf:: Text> runTxt   = std:: make_shared<sf:: Text>("FLEE", liveData:: fontsList["GLOBALFONT"]);
        std:: shared_ptr<sf:: Text> bossName = std:: make_shared<sf:: Text>("OCULUS", liveData:: fontsList["GLOBALFONT"]);

        std:: shared_ptr<sf:: Sprite> parallaxBaka = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX000"]);
        std:: shared_ptr<sf:: Sprite> parallaxMida = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX001"]);
        std:: shared_ptr<sf:: Sprite> parallaxFrta = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX002"]);
        std:: shared_ptr<sf:: Sprite> parallaxFlra = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX003"]);
        std:: shared_ptr<sf:: Sprite> parallaxBakb = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX000"]);
        std:: shared_ptr<sf:: Sprite> parallaxMidb = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX001"]);
        std:: shared_ptr<sf:: Sprite> parallaxFrtb = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX002"]);
        std:: shared_ptr<sf:: Sprite> parallaxFlrb = std:: make_shared<sf:: Sprite>(liveData:: texList["PARALLAX003"]);

        std:: shared_ptr<sf:: Sprite> hero        = std:: make_shared<sf:: Sprite>(liveData:: texList["HERO"], heroRect);
        std:: shared_ptr<sf:: Sprite> armyChick   = std:: make_shared<sf:: Sprite>(liveData:: texList["ARMYCHICK"], dataObj->imgSrcRects["MILITARYCHICKBATTLE"]);
        std:: shared_ptr<sf:: Sprite> eyeball     = std:: make_shared<sf:: Sprite>(liveData:: texList["MONSTERS"], dataObj->imgSrcRects["EYEMON"]);
        std:: shared_ptr<sf:: Sprite> heroStatus  = std:: make_shared<sf:: Sprite>(liveData:: texList["GUI"], dataObj->imgSrcRects["TEXTRECT"]);
        std:: shared_ptr<sf:: Sprite> statusName  = std:: make_shared<sf:: Sprite>(liveData:: texList["GUI"], dataObj->imgSrcRects["TEXTRECT"]);
        std:: shared_ptr<sf:: Sprite> attackIcon  = std:: make_shared<sf:: Sprite>(liveData:: texList["ICONS"], dataObj->imgSrcRects["ICONFIST"]);
        std:: shared_ptr<sf:: Sprite> defendIcon  = std:: make_shared<sf:: Sprite>(liveData:: texList["ICONS"], dataObj->imgSrcRects["ICONSHIELD"]);
        std:: shared_ptr<sf:: Sprite> spellIcon   = std:: make_shared<sf:: Sprite>(liveData:: texList["ICONS"], dataObj->imgSrcRects["ICONBOOK"]);
        std:: shared_ptr<sf:: Sprite> runIcon     = std:: make_shared<sf:: Sprite>(liveData:: texList["ICONS"], dataObj->imgSrcRects["ICONRUN"]);
        std:: shared_ptr<sf:: Sprite> cursor      = std:: make_shared<sf:: Sprite>(liveData:: texList["GUI"], dataObj->imgSrcRects["CURSOR"]);
        std:: shared_ptr<sf:: Sprite> heroHP      = std:: make_shared<sf:: Sprite>(liveData:: texList["HPBAR"]);
        std:: shared_ptr<sf:: Sprite> heroineHP   = std:: make_shared<sf:: Sprite>(liveData:: texList["HPBAR"]);
        std:: shared_ptr<sf:: Sprite> bossHP      = std:: make_shared<sf:: Sprite>(liveData:: texList["HPBAR"]);

        //1280x720
        parallaxBaka->scale(1280/1280, 720/720);
        parallaxMida->scale(1280/1280, 720/720);
        parallaxFrta->scale(1280/1280, 720/720);
        parallaxFlra->scale(1280/1280, 720/720);
        parallaxBakb->scale(1280/1280, 720/720);
        parallaxMidb->scale(1280/1280, 720/720);
        parallaxFrtb->scale(1280/1280, 720/720);
        parallaxFlrb->scale(1280/1280, 720/720);
        hero->scale(4,4);
        armyChick->scale(4, 4);
        eyeball->scale(10,10);
        heroStatus->scale(3.2, 2.8);
        //statusName->scale(.8, 1.5);
        //monsterStatus.scale(2, 1.5);
        attackIcon->scale(.8, .8);
        defendIcon->scale(.8, .8);
        spellIcon->scale(.8, .8);
        runIcon->scale(.8, .8);
        cursor->scale(2.5, 2.5);
        bossHP->scale(2.6, 2);

        // (left/right, up/down)
        parallaxMida->move(0, -75);
        parallaxFrta->move(0, -75);
        parallaxMidb->move(1280, -75);
        parallaxFrtb->move(1280, -75);
        parallaxBakb->move(1280, 0);
        parallaxFlrb->move(1280, 0);
        hero->move(700, 505);
        armyChick->move(815, 495);
        eyeball->move(270, 220);
        heroStatus->move(SCREEN_WIDTH - heroStatus->getLocalBounds().height - 10, 20);
        statusName->move(1085, 625);
        //monsterStatus.move(450, 120);
        attackIcon->move(1115, 50);
        defendIcon->move(1110, 198);
        spellIcon->move(1112, 346);
        runIcon->move(1112, 494);
        cursor->move(1110 , 81);
        atkTxt->setPosition( (SCREEN_WIDTH / 2) - ((atkTxt->getLocalBounds().left + atkTxt->getLocalBounds().width) / 2), 40);
        defTxt->setPosition( (SCREEN_WIDTH / 2) - ((defTxt->getLocalBounds().left + defTxt->getLocalBounds().width) / 2), 40);
        magTxt->setPosition( (SCREEN_WIDTH / 2) - ((magTxt->getLocalBounds().left + magTxt->getLocalBounds().width) / 2), 40);
        runTxt->setPosition( (SCREEN_WIDTH / 2) - ((runTxt->getLocalBounds().left + runTxt->getLocalBounds().width) / 2), 40);
        bossName->setPosition( (SCREEN_WIDTH / 2) - ((bossName->getLocalBounds().left + bossName->getLocalBounds().width) / 2), 40);
        heroHP->setPosition(0, 50);
        heroineHP->setPosition(0, heroHP->getGlobalBounds().height + 50);
        bossHP->setPosition(eyeball->getGlobalBounds().left - 20, eyeball->getGlobalBounds().top - 30);

        heroStatus->rotate(90);

        atkTxt->setCharacterSize(27);
        defTxt->setCharacterSize(27);
        magTxt->setCharacterSize(27);
        runTxt->setCharacterSize(27);
        bossName->setCharacterSize(27);

        liveData:: textObjsList.emplace("ATTACK", std:: move(atkTxt) );
        liveData:: textObjsList.emplace("GUARD", std:: move(defTxt) );
        liveData:: textObjsList.emplace("MAGIC", std:: move(magTxt) );
        liveData:: textObjsList.emplace("FLEE", std:: move(runTxt) );
        liveData:: textObjsList.emplace("OCULUS", std:: move(bossName) );

        liveData:: spriteList.emplace( "001_PARALLAXBCKA", std:: move(parallaxBaka) );
        liveData:: spriteList.emplace( "002_PARALLAXBCKB", std:: move(parallaxBakb) );
        liveData:: spriteList.emplace( "003_PARALLAXMIDA", std:: move(parallaxMida) );
        liveData:: spriteList.emplace( "004_PARALLAXMIDB", std:: move(parallaxMidb) );
        liveData:: spriteList.emplace( "005_PARALLAXFLRA", std:: move(parallaxFlra) );
        liveData:: spriteList.emplace( "006_PARALLAXFLRB", std:: move(parallaxFlrb) );
        liveData:: spriteList.emplace( "007_PARALLAXFRTA", std:: move(parallaxFrta) );
        liveData:: spriteList.emplace( "008_PARALLAXFRTB", std:: move(parallaxFrtb) );

        liveData:: spriteList.emplace("012_ARMYGIRLBATTLE", std:: move(armyChick) );
        liveData:: spriteList.emplace("011_HEROBATTLE", std:: move(hero) );
        liveData:: spriteList.emplace("010_EYEBALL", std:: move(eyeball) );

        liveData:: spriteList.emplace("030_MENUBOX", std:: move(heroStatus) );
        liveData:: spriteList.emplace("031_ATKICON", std:: move(attackIcon) );
        liveData:: spriteList.emplace("032_DEFICON", std:: move(defendIcon) );
        liveData:: spriteList.emplace("033_MAGICON", std:: move(spellIcon) );
        liveData:: spriteList.emplace("034_RUNICON", std:: move(runIcon) );
        liveData:: spriteList.emplace("035_CURSOR", std:: move(cursor) );
        liveData:: spriteList.emplace("036_STATUSNAME", std:: move(statusName) );
        liveData:: spriteList.emplace("037_HEROHP", std:: move(heroHP) );
        liveData:: spriteList.emplace("038_HEROINEHP", std:: move(heroineHP) );
        liveData:: spriteList.emplace("009_BOSSHP", std:: move(bossHP) );       // made this a higher priority than the boss, for aesthetic reasons

        liveData:: renderSprites = liveData:: spriteList;
        liveData:: renderSprites.erase("002_PARALLAXBCKB");
        liveData:: renderSprites.erase("004_PARALLAXMIDB");
        liveData:: renderSprites.erase("006_PARALLAXFLRB");
        liveData:: renderSprites.erase("008_PARALLAXFRTB");
        liveData:: renderSprites.erase("036_STATUSNAME");


        sf:: SoundBuffer blipBuffer;
        sf:: SoundBuffer selectBuffer;
        sf:: SoundBuffer heroStrikeBuffer;
        blipBuffer.loadFromFile(dataObj->paths["BLIP"]);
        selectBuffer.loadFromFile(dataObj->paths["MENUSELECT"]);
        heroStrikeBuffer.loadFromFile(dataObj->paths["HEROSTRIKE"]);
        sf:: Sound blipSound;
        sf:: Sound selectSound;
        sf:: Sound hStrikeSound;

        liveData:: buffersList.emplace("BLIPBUFFER", blipBuffer);
        blipSound.setBuffer(liveData:: buffersList["BLIPBUFFER"]);
        liveData:: soundsList.emplace("BLIPSOUND", blipSound);

        liveData:: buffersList.emplace("SELECTBUFFER", selectBuffer);
        selectSound.setBuffer(liveData:: buffersList["SELECTBUFFER"]);
        liveData:: soundsList.emplace("MENUSELECT", selectSound);

        liveData:: buffersList.emplace("HSTRIKEBUFFER", heroStrikeBuffer);
        hStrikeSound.setBuffer(liveData:: buffersList["HSTRIKEBUFFER"]);
        liveData:: soundsList.emplace("HSTRIKE", hStrikeSound);

        liveData:: soundsList["HSTRIKE"].setPitch(.75);

        createActorObject("011_HEROBATTLE", "037_HEROHP", 100);
        createActorObject("012_ARMYGIRLBATTLE", "038_HEROINEHP", 100);    //WHAT THE FUCK DID YOU DO??
        createActorObject("010_EYEBALL", "009_BOSSHP", 150);
    }
}

#endif
