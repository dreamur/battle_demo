/*************************
 *
 * Animation header
 *
 * BRIEF:
 *
 *
 * TODO:
 *
 *
 ***************************/

#ifndef __ANIMATION_H__
#define __ANIMATION_H__

#include <functional>
#include <iostream>
#include "data.h"
#include "actor.h"

class animation
{

    public:
        int scaleX, scaleY, frequency, xOffset, yOffset;    // for the lambdas

        int age;
        int previousTime;
        unsigned lifeTime;    // this will be measured in milliseconds

        // choosing to make each animation object
        // responsible for a single sprite or text obj ONLY
        // for the time being (instead of a variable
        // # with a container of some sort).
        std:: shared_ptr<sf:: Sprite> moveableSprite;
        std:: shared_ptr<sf:: Text> moveableText;
        std:: string spriteString;

        std:: function<void(int, int*, int*)> utilFn;
        std:: function<void(int, int*, int*, int, int)> moveFn;
        std:: function<void(int, int, int, int)> scaleFn;

        animation() = default;
        animation(std:: string keyName, unsigned lifeSpan, bool isActor = false, bool isText = false);
        animation(const animation& ani);
        animation& operator = (const animation& ani);
        ~animation() = default;

        void update();
        void setScale(int x, int y);
        void setOffsets(int x, int y);
        void setFrequency(int f);

};

inline animation:: animation(const animation& ani)
{
    age             = ani.age;
    previousTime    = ani.previousTime;
    lifeTime        = ani.lifeTime;
    moveableSprite  = ani.moveableSprite;
    moveableText    = ani.moveableText;
    spriteString    = ani.spriteString;
    scaleX          = ani.scaleX;
    scaleY          = ani.scaleY;
    frequency       = ani.frequency;
    xOffset         = ani.xOffset;
    yOffset         = ani.yOffset;
    utilFn          = ani.utilFn;
    moveFn          = ani.moveFn;
    scaleFn         = ani.scaleFn;

    //std:: cout << "copy ctr called\n";
}

inline animation:: animation(std:: string keyName, unsigned lifeSpan, bool isActor, bool isText)
{
    if (isActor) // if is a pc or monster
    {
        moveableSprite  = liveData:: actorList.at(keyName)->actorImage;
        moveableText    = nullptr;
    }
    else if (!isActor && !isText) // if not one of the pcs or monster or text
    {
        moveableSprite  = liveData:: renderSprites.at(keyName);
        moveableText    = nullptr;
    }
    else if (!isActor && isText) // if not one of pcs or monster, but is text
    {
        moveableSprite  = nullptr;
        moveableText    = liveData:: renderText.at(keyName);
    }
    // there will never a state for actor && text

    lifeTime        = lifeSpan;
    spriteString    = keyName;
    scaleX = scaleY = frequency = xOffset = yOffset = 0;
    utilFn          = nullptr;
    moveFn          = nullptr;
    scaleFn         = nullptr;
    age             = 0;
    previousTime    = liveData:: battleTimer.getElapsedTime().asMilliseconds();

    //std:: cout << "ctr called\n";
}

inline animation& animation:: operator = (const animation& ani)
{
    if (this != &ani)
    {
        moveableSprite.reset();
        moveableText.reset();

        age             = ani.age;
        lifeTime        = ani.lifeTime;
        moveableSprite  = ani.moveableSprite;
        moveableText    = ani.moveableText;
        spriteString    = ani.spriteString;
        scaleX          = ani.scaleX;
        scaleY          = ani.scaleY;
        frequency       = ani.frequency;
        xOffset         = ani.xOffset;
        yOffset         = ani.yOffset;
        utilFn          = ani.utilFn;
        moveFn          = ani.moveFn;
        scaleFn         = ani.scaleFn;
        previousTime    = ani.previousTime;
    }

    std:: cout << "assignment called\n";

    return *this;
}

inline void animation:: update()
{
    if (utilFn != nullptr) { utilFn(frequency, &age, &previousTime); }
    if (moveFn != nullptr) { moveFn(frequency, &age, &previousTime, xOffset, yOffset); }
    if (scaleFn != nullptr) { scaleFn(frequency, age, scaleX, scaleY); }
}

inline void animation:: setScale(int x = 0, int y = 0)
{
    scaleX = x;
    scaleY = y;
}
inline void animation:: setOffsets(int x = 0, int y = 0)
{
    xOffset = x;
    yOffset = y;
}
inline void animation:: setFrequency(int f = 0)
{
    frequency = f;
}

#endif
